﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace PaiCalculator
{
    public partial class PaiCalculatorPage : ContentPage
    {
        int currentState = 1;
        string mathOperator;
        double firstNumber, secondNumber;

        public PaiCalculatorPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PaiCalculator)}:  constructor");
            InitializeComponent();
            ClickClear(this, null);
        }

        void ClickNum(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if (this.AnswerDisplayLabel.Text == "0" || currentState < 0)
            {
                this.AnswerDisplayLabel.Text = "";
                if (currentState < 0)
                    currentState *= -1;
            }

            this.AnswerDisplayLabel.Text += pressed;

            double number;
            if (double.TryParse(this.AnswerDisplayLabel.Text, out number))
            {
                this.AnswerDisplayLabel.Text = number.ToString("N0");
                if (currentState == 1)
                {
                    firstNumber = number;
                }
                else
                {
                    secondNumber = number;
                }

            }
        }

        void ClickOperator(object sender, EventArgs e)
        {
            currentState = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            mathOperator = pressed;
        }

        void ClickClear(object sender, EventArgs e)
        {
            firstNumber = 0;
            secondNumber = 0;
            currentState = 1;
            this.AnswerDisplayLabel.Text = "0";
        }

        static double Arithmatic(double value1, double value2, string mathOperator)
        {
            double result = 0;

            switch (mathOperator)
            {
                case "/":
                    result = value1 / value2;
                    break;
                case "×":
                    result = value1 * value2;
                    break;
                case "+":
                    result = value1 + value2;
                    break;
                case "-":
                    result = value1 - value2;
                    break;
            }

            return result;
        }


        void ClickEqual(object sender, EventArgs e)
        {
            if (currentState == 2)
            {
                var result = Arithmatic(firstNumber, secondNumber, mathOperator);

                this.AnswerDisplayLabel.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
            }
        }
    }
}
